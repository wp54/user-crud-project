import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false)
  const message = ref("");
  const timeout = ref(2000);
  const shgowMessage = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  }
  const closeMessage = (msg: string) => {
    message.value = msg;
    isShow.value = false;
  }
  return { isShow, message, shgowMessage, closeMessage, timeout };
});
